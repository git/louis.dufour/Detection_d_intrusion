regles=[]

## How to write rules :

# A rule is an array of two values :

# The first one is another array of conditions
# The second one is the forbidden value

regle1=[[('r',5,55),('c',3,1),('c',12,0)],('r',8,72)]

# regle1 is read as follows :
# If the register at the address 5 has a value of 55, and the values of the coils at the addresses 3 and 12 are 1 and 0 respectively
# Then any request trying to set the value of the register at the address 8 to 72 is blocked.

regle2=[[('r',5,55),('c',3,1),('c',12,0)],('c',9,1)]

regles.append(regle1)
regles.append(regle2)




# getRegles()'s only purpose is to not have the regles variable called in other scripts, so that it isn't overwritten by accident
def getRegles():
    return regles
