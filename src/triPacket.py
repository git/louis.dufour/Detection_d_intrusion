from ecritureBDD import ecritureBDD

def decoupePacket(lPkt):
    ''' decoupePacket is a recursive function that takes a list as an argument.
    Its purpose is to create lists that shows the updated statuses from the modbus client's requests following the database format.
    '''
    if type(lPkt)!=list:
        # If the list of statuses isn't a list, nothing should be put inside the database.
        print('pas liste')
        return
    if len(lPkt)==0:
        # If the list is empty, nothing should be put inside the database as well.
        print('liste vide')
        return
    if len(lPkt)==1:
        # If the list has only one element, it is probably another list inside, that's why we call decoupePacket again.
        return decoupePacket(lPkt[0])
    if len(lPkt)==3 and type(lPkt[0])==str:
        # If the list has a size of 3, and it's first element is a string type, we probably have the status we need to use.
        return [[lPkt[0],int(lPkt[1]),int(lPkt[2])]]
    else:
        l=[]
        for i in lPkt:
            l+=decoupePacket(i)
        return l

def triPacket(lPkt,connec):
    '''
    triPacket takes the list of statuses obtained from the client's request and the information needed to connect to the database.
    '''
    lNettoyee=decoupePacket(lPkt)
    # decoupePacket is called in order to create a better structured list of statuses.
    ecritureBDD(lNettoyee,connec)
    # ecritureBDD is now called to update the values inside the database.
