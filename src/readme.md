# Client commands :

```
client.write_coil address=0 value=0 slave=1
client.write_coil address=0 value=1 slave=1

client.write_coils address=0 values=1,0,1,1,0 slave=1
client.write_coils address=0 values=0,0,0 slave=1

client.read_coils address=0 count=7 slave=1
client.read_coils address=0 count=9 slave=1

client.write_register address=0 value=52 slave=1
client.write_registers address=0 values=845,123,0,427,4 slave=1

client.read_holding_registers address=0 count=1 slave=1
client.read_holding_registers address=0 count=100 slave=1
```

