/*========================================================================
Script        : Table.sql
Author        : Dufour Louis
Creation date : 20-02-2023
========================================================================*/

DROP TABLE IF EXISTS Status;

CREATE TABLE IF NOT EXISTS Status
(
    addresse numeric, 
    type char(1),
    valeur numeric,
    primary key(addresse,type)
);