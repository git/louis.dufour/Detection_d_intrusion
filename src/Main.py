
# ========================================================================
#
# Script        : Main.py
# Author        : Dufour Louis
# Creation date : 20-02-2023
#
# ========================================================================

import psycopg2 as psy# pip3 install types-psycopg2
import pandas as pd # pip3 install pandas
import getpass

################# Debut Tips #################

# Pour lancer le script il faut être dans le réseau de l'iut
# Il faut aussi se trouver dans le répertoire où se trouvent les fichiers sinon le script ne trouve pas les fichiers sql

################# Fin Tips #################


def create_tables(connection, filename):
    cur = connection.cursor()
    with open(filename) as f:
        cur.execute(f.read())
    connection.commit()
    cur.close()

# Lors de l'execution le main s'execute et en plus si on l'import autre part cela n'executera pas le code
if __name__ == '__main__':
    db_host = input('Nom d\'hôte : ')
    if not db_host:
        db_host = 'londres'
    db_name = input('Nom de la base de données : ')
    if not db_name:
        db_name = 'dblodufour1'
    db_user = input('Utilisateur : ')
    if not db_user:
        db_user = 'ladufour1'
    db_password = getpass('Mot de passe : ')

    connection = psy.connect(host=db_host, database=db_name, user=db_user, password=db_password)
    
    
    create_tables(connection, 'Table.sql')
    connection.close() 