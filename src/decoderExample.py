#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scapy.all as scapy
import scapy.contrib.modbus as mb

def decode(pkt):
    if "ModbusADU" in pkt:
        pkt["ModbusADU"].show()
        print("--------------------")

scapy.sniff(iface="lo", prn=decode)

