# [Détection d'intrusion](https://codefirst.iut.uca.fr/git/cedric.bouhours/Projets_SAE_S4/src/commit/a03fca3b1d447d2b86a180fa760204513c56e919/Projets/Projet_07.md)

## Utilisation

### Install

```sh
// Install d'outil
python3 -m pip install pipenv

// Install module
pip install typer
pip install scapy
pip install pymodbus

// Mise à jour
pipenv update

// Lancement du shell pipenv
pipenv shell
```
**ATTENTION** : lorsque vous voulez faire un pipenv update cela vous donne une version buguée de pymodub qui est là `3.2.0` *(pour voir votre version faite un `pip freeze`)*

#### Marche à suivre:
1) Désinstallez la bibliothèque pymodbus à l'aide de la commande suivante:
> pipenv uninstall pymodbus

2) Installez une version précédente de la bibliothèque pymodbus en utilisant la commande suivante:
> pipenv install pymodbus==3.1.3

### Lancement
il faut lancer 2 terminaux avec `pipenv shell` :

```sh
./start_server.sh
./start_client.sh
```
puis pour lancer la sonde (sous root) :
```sh
sudo python3 ./decoderMain.py
```

### Côté BDD 
Il faudra que vous connectiez à votre BDD PostgreSQL.
Exécuter le script `Table.sql` qui se trouve dans src avec la commande ci-dessous.
```sh
\i /YOUR_PATH/Detection_d_intrusion/src/Table.sql
```

Si jamais vous vous retrouvez à devoir partager votre BDD il faudra exécuter la commande ci-dessous.
```sh
GRANT ALL ON <nom_table> TO <nom_utilisateur_à_ajouter>;
GRANT CONNECT ON DATABASE dblodufour1 TO <nom_utilisateur_à_ajouter>;
```

Pour se connecter à la BDD d'une autre personne pensée bien à mettre le nom de sa database.
Ici, c'est `dblodufour1`
```sh
psql -h londres -d <nom_DataBase> -U <votre_nom_utilisateur> -W
```

**ATTENTION** `londres` est un serveur héberger dans l'infrastructure de notre établissement universitaire.

## Notre configuration
* Python (3.9)
* PostgreSQL
* pip (22.0.2)
* pymodbus (3.1.3)

Notre pipfile:
```
[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"

[packages]
redis = "*"
click = "*"
prompt-toolkit = "*"
pymodbus = {extras = ["repl"], version = "*"}
sqlalchemy = "*"
scapy = "*"
ipython = "*"

[dev-packages]

[requires]
python_version = "3.9"
```

## Développeurs 
* [Louis](https://codefirst.iut.uca.fr/git/louis.dufour)
* [Paul](https://codefirst.iut.uca.fr/git/paul.squizzato)
* [Eloan](https://codefirst.iut.uca.fr/git/eloan.andre)
* [Darius](https://codefirst.iut.uca.fr/git/darius.bertrand)
