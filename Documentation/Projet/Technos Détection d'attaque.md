# Comparatif des différentes technos pour détection d'intrusion

## Point vocabulaire
- NSM : Network and System Management
- IDS : Intrusion Detection System
- IPS : Intrusion Prevention System
- NIDS : Network Intrusion Detection System
- IDMEF : Intrusion Detection Message Exchange Format 
- SIEM : Security Information and Event Management

## Quelle technos ?

\ | Avantages | Inconvénients | Objectif
 --- | --- | --- | --- 
[Snort](https://www.snort.org/#documents) | - Surveille l'état des serveurs en temps réel en surveillant les fichiers de configuration, les journaux systèmes et d'autres... <br> - Gratuit et open-source <br> - Flexibilité *(peut-être configuré pour détecter un grand nombre de types de menaces)* <br> - Grande communauté <br> - Performant *(Analyse de grandes quantités de données en temps réel)* | - Configuration Complexe <br> - False positifs *(Peut générer de nombreux faux positifs, ce qui peut entraîner un gaspillage de temps et de ressources pour les administrateurs)* <br> - Nécessite une maintenance régulière | Ce logiciel a pour principale fonction IDS et IPS.
[Suricata](https://suricata.readthedocs.io/en/suricata-6.0.9/) | - Architecture plus moderne et des fonctionnalités de détection avancées telles que la détection de menaces SSL/TLS <br> - Détection en temps réel <br> - Support multi-plateforme *(macOS, Windows et Linux)* <br> - Intégration avec plusieurs systèmes *(SIEM)* <br> - Mise à jour automatique des signatures de menace  | - Complexité d'installation et de configuration <br> - Exigences de ressources élevées *(peu rendre les systèmes lent)* <br> - Faible performance dans les environnements de grande envergure | Ce logiciel a pour principale fonction IDS, IPS et NSM.
[Zeek (Bro)](https://docs.zeek.org/en/master/) | - Surveiller l'état des serveurs en temps réel en surveillant les fichiers de configuration, les journaux système et d'autres... <br> - Peut être configuré pour surveiller le trafic en temps réel <br> - Gratuit et open-source <br> - Grande communauté <br> - Flexibilité (analyse poussé) <br> - Puissance (grande) | - Complexité de programmation *(Nécessite une formation)* <br> - Utilisation de ressources élevées *(peu rendre les systèmes lent)* <br> - Limites dans la détection des menaces <br> - Maintenance lourd | Ce logiciel a pour principale fonction NIDS et NSM.

Il existe d'autres logiciels qui peuvent être considérés comme des alternatives de qualité. Voici quelques exemples :
> **Security Onion** : c'est une distribution Linux qui regroupe plusieurs outils de sécurité, dont Snort, Suricata et Zeek. Elle est conçue pour la surveillance et l'analyse du trafic réseau.

> **Moloch** : c'est un outil d'analyse de trafic réseau open source qui permet de stocker et d'indexer de grandes quantités de données de trafic réseau. Il peut être utilisé pour la recherche de menaces et l'investigation d'incidents.

> **Sagan** : c'est un outil de détection d'intrusion qui utilise les règles de Snort pour identifier les menaces. Il est conçu pour être plus rapide que Snort et pour consommer moins de ressources système.

> **Suricata-IDS** : c'est une alternative à Suricata qui propose des fonctionnalités similaires, mais avec une architecture plus moderne.

> **Bro-IDS** : c'est une alternative à Zeek qui propose des fonctionnalités similaires, mais avec une architecture plus moderne.

> **OSSEC** : c'est un système de détection d'intrusion open source qui surveille l'intégrité des fichiers, les journaux système, le trafic réseau et d'autres aspects de la sécurité du système en temps réel.

> **Wazuh** : c'est une alternative à OSSEC qui offre des fonctionnalités similaires, mais avec une interface utilisateur améliorée et des fonctionnalités supplémentaires telles que la détection d'attaques de type "fileless". Il inclut également des outils pour la gestion des alertes et la gestion des politiques de sécurité.

## Conclusion
En tant qu'étudiant en informatique, il est recommandé de se familiariser avec des outils de sécurité populaires et utilisés dans l'industrie. Les outils de système de détection d'attaques tels que `Snort`, `Suricata`, `Zeek(Bro)` et `OSSEC` sont des exemples de ces outils.

Cela étant dit, il n'y a pas de réponse unique à la question de savoir quel est le meilleur outil de système de détection d'attaque pour un étudiant en informatique. Le choix dépendra de l'objectif de l'étudiant et du domaine d'intérêt. Par exemple, si l'étudiant s'intéresse à la détection d'attaques réseau, `Snort`, `Suricata` et `Zeek` peuvent être de bons choix. Si l'étudiant s'intéresse à la détection d'attaques de système, `OSSEC` peut être un meilleur choix.

En général, il est recommandé de commencer par un outil de sécurité qui est bien documenté, facile à installer et à configurer, et qui a une communauté active. Cela permettra à l'étudiant de se concentrer sur l'apprentissage des concepts de sécurité, plutôt que de passer du temps à essayer de comprendre un outil complexe.



