# Notre Architecture Technologique
![Image clique droit](/Schemas/Architecture_Projet.png)

**Les technologies que nous utiliserons lors du projet :**
* [Python :](https://www.python.org/) offre énormément de librairie afin de faire de la sécurité réseau, mais aussi de mettre en place une communication avec la bases de données.
* [Scapy :](https://scapy.net/) offre la possibilité de voir/capturer le trafique d'une communication entre notre serveur et notre client. *(il est l'équivalent de Wireshark, mais adapté à python)*
* [Postgresql :](https://www.postgresql.org/) base de données énormément utilisée lors de notre apprentissage scolaire avec des serveurs fournis par notre établissement afin de réaliser nos tests.
* [Psycopg2 :](https://pypi.org/project/psycopg2/) librairie python qui nous permet de réaliser la liaison entre notre base de données et notre code.
* [Pymodbus :](https://pypi.org/project/pymodbus/) librairie python qui nous permet la création d'un client et d'un serveur via des scripts python.
* [Wazuh :](https://wazuh.com/) Pour intégrer Wazuh avec Python, vous pouvez utiliser l'API REST fournie par Wazuh pour accéder aux fonctionnalités de Wazuh.

# Pourquoi pas Snort ou Suricata ?
La surveillance en temps réel des valeurs de variables qui prenne en compte l'état précédent n'est pas une fonctionnalité courante des systèmes de détection d'intrusion et peut nécessiter des outils de débogage spécifiques pour être réalisée.

La meilleure solution pour surveiller en temps réel les modifications de variables est d'utiliser des outils de débogage avancés qui permettent de capturer des informations sur les processus en cours d'exécution.

