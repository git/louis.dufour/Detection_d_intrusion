## Rendez-vous du 7 Février

- Utilisation de Pymodbus (exemples dans le répertoire Pymodbus à la racine du projet).
    - start_server.sh lance un serveur Pymodbus sur le port 502 (utilisation de pipenv)
    - start_client.sh lance le client Pymodbus sur le port 502, on peut s'en servir pour simuler l'attaquant en communiquant avec le serveur.
    - Decoder.py recupère les requêtes lancées par le client avec Scapy sur le port 502. Pour l'instant il affiche toutes les informations, mais il devra être modifié ou utilisé pour faire ressortir les messages malveillants.

### À faire :
 À l'aide de Pymodbus, faire sortir les informations qu'on souhaite regarder.  
 Rapport du 9 Février.

#### Prochain rendez-vous prévu le lundi 20 Février entre 13h30 et 15h30