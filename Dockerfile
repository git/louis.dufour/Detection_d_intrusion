FROM python:3.9-slim-buster

WORKDIR /app

COPY src /app/src
COPY requirements.txt /app/
#COPY setup.py /app/

RUN python -m venv venv
RUN . venv/bin/activate && pip install -r requirements.txt && python setup.py install

RUN python -m pytest
#RUN python setup.py sdist bdist_wheel

CMD ["python", "app.py"]
